import React from 'react';


const UserList = (props) => {
      const users = props.list;
       
     const singleUser = users.map(({id,name}) => {
         return (  
              <li key={id} className='list-group-item'>
                      {id} - {name}
            </li>
         )
        })

        return (
            
                <ul className='list-group'>
                    {singleUser}
                </ul>
            
        )
  };

  export default UserList;