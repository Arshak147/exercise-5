import React from 'react';
import UserList from './UserList'

class App extends React.Component {
   state = {
     users : [
        { id: 1, name: "Sam Smith" },
        { id: 2, name: "Lisa More" },
        { id: 3, name: "David Cohen" },
        { id: 4, name: "Jim Taylor" },
      ]
   }
    render() {
        return(
        <div className= "container">
            <UserList list = {this.state.users}/>
        </div>
        )
    }
}

export default App;